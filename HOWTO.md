# Requisitos

1. Verificar o servido para o funcionamento da aplicação(Foi usado Apache tomcat na porta 8080);
2. Verificar o banco de dados;
3. No navegador use "http://localhost:8080/funcionarios/";


# O que foi feito

1. Implementações dos metodos de inserir, atualizar e deletar;
2. Efetuada teste com postmam;
3. Iniciado o desenvolvimento das paginas com axios para todas os metodos;
