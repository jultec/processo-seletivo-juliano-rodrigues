package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;

class FuncionarioServiceTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}
	
	@Test
	void testFuncionarioRead() {
		fail("Not yet implemented");
	}

	@Test
	void testFuncionarioCreate() {
		FuncionarioDTO funcionario = new Funcionario();

		funcionario.setNome("Usuário criado");
		funcionario.setSalario(new BigDecimal(5000));
		funcionario.setEmail("testecreate@teste.com");
		funcionario.setIdade(33);

	}

	@Test
	void testFuncionarioUpdate() {
		FuncionarioDTO funcionarioDTO = new FuncionarioDTO();

		funcionario.setId(2L);
		funcionario.setEmail("teste@teste.com");
		funcionario.setIdade(22);
		funcionario.setSalario(new BigDecimal(1500));
		funcionario.setNome("Teste de update");

		Setor setor = new Setor();
		setor.setId(2L);

		funcionario.setSetor(setor);


	@Test
	void testFuncionarioDelete() {
		funcionario.funcionarioDelete(2L);
	}

}
